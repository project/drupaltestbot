api = 2
core = 6.x
; Standard D6 simpletest patch
projects[drupal][patch][simpletest_core] = http://drupalcode.org/project/simpletest.git/blob_plain/refs/heads/6.x-2.x:/D6-core-simpletest.patch
; Increase timeout on drupal_http_request() per http://drupal.org/node/1830742
projects[drupal][patch][1830742] = http://drupal.org/files/1830742-increase-timeout.patch

projects[] = coder_tough_love

projects[coder][download][type] = git
projects[coder][download][tag] = 6.x-2.0-rc1

projects[] = admin_menu
projects[] = module_filter
projects[] = adminrole
projects[project_issue_file_review][download][type] = git
projects[project_issue_file_review][download][branch] = 6.x-2.x

; The simpletest revision here is where the new formfield types
; were introduced. When a release later than Simpletest 6.x-2.11 is made,
; we can use that.
projects[simpletest][download][type] = git
projects[simpletest][download][revision] = c4d3a3ec
